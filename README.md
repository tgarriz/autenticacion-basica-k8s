# Autenticacion Basica K8s 

Surge la necesidad de exponer la gui de longhorn para la adiministracion de backups de PVCs por parte de un agente externo().
Para securizar el ingress establesco una passwd en el ingress y un rango de IPs desde donde el ingress aceptara los request.

## Instalar htpasswd y generar credenciales

Instalamos el aplicativo htpasswd y generamos el file de credenciales
```bash
htpasswd -c auth admin
```
Si necesitamos agregar es la misma sentencia pero sin el -c, esto genera un file con nombre auth que contiene user y pass encriptado

## Creamos un secret a partir del file de credenciales

```bash
kubectl -n longhorn-system create secret generic basic-auth --from-file=auth
```

## Agregamos los siguientes annotations en el ingress 

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    nginx.ingress.kubernetes.io/auth-realm: Authentication Required
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-type: basic
spec:
  ingressClassName: nginx
  rules:
    - host: longhorn.arba.gov.ar
      http:
        paths:
          - backend:
              service:
                name: longhorn-frontend
                port:
                  number: 80
            path: /
            pathType: Prefix
```

## Ademas se podria agregar un rango de ips desde las cuales el ingress aceptaria http request

Agregamos un annotations 
nginx.ingress.kubernetes.io/whitelist-source-range: '64.94.14.0/27,64.94.228.128/28,216.52.39.0/24'

